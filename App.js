import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import Navigator from './src/Navigator';

const App = () => {
  return (
    <Navigator />
  );
};

export default App;
