// __tests__/Main-test.js
import React from 'react';
import Photo from '../src/screens/Photo';

import renderer from 'react-test-renderer';

test('renders correctly', () => {
  const tree = renderer.create(<Photo />).toJSON();
  expect(tree).toMatchSnapshot();
});