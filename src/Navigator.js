import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './screens/Main';
import PhotoScreen from './screens/Photo';

const Stack = createStackNavigator();

function MainStack() {
    return (
        <Stack.Navigator
            initialRouteName="Main" >
            <Stack.Screen
                name="Main"
                component={MainScreen}
                options={() => ({
                    headerTitleAllowFontScaling: false,
                    headerTitle: (
                        <Text
                            style={styles.title}>
                            Razas de Perro
                        </Text>
                    )
                })} />
            <Stack.Screen
                name="Photo"
                component={PhotoScreen}
                options={() => ({
                    headerTitleAllowFontScaling: false,
                    headerTitle: (
                        <Text
                            style={styles.title}>
                            Galería de Fotos
                        </Text>
                    )
                })} />
        </Stack.Navigator >
    );
}

export default function Navigator() {
    return (
        <NavigationContainer>
            <MainStack />
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        color: '#000',
        letterSpacing: .5
    }
});
