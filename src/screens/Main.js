import React, { Fragment, useEffect, useState } from 'react';
import { Dimensions, FlatList, RefreshControl, StyleSheet, View, Text, TouchableOpacity } from 'react-native';

const { width, height } = Dimensions.get('window');

const Main = ({ navigation }) => {

    const [refreshing, setRefreshing] = useState(false);
    const [listDogs, setListDogs] = useState({});

    useEffect(() => {
        loadBreedDogs();
    }, []);

    const loadBreedDogs = async () => {
        try {
            let response = await fetch(`https://dog.ceo/api/breeds/list/all`, {
                method: 'GET'
            });
            let result = await response.json();
            if (result.status === 'success') {
                let arrayBreeds = [];
                let breeds = result.message;
                let keys = Object.keys(breeds);
                let keyBreed = 0;
                for (let index = 0; index < keys.length; index++) {
                    const element = keys[index];
                    if (breeds[element].length > 0) {
                        keys.splice(index, 1);
                        index--;
                        for (const subBreed of breeds[element]) {
                            arrayBreeds.push({ id: keyBreed, name: `${subBreed} ${element}`, value: `${element}/${subBreed}` });
                            keyBreed++;
                        }
                    } else {
                        arrayBreeds.push({ id: keyBreed, name: element, value: element });
                        keyBreed++;
                    }
                }
                setListDogs(arrayBreeds);
            } else {
                setListDogs([]);
                console.log("Error getting documents:");
            }
        } catch (error) {
            console.log(error);
        }
    }

    const handlePress = (breed) => {
        navigation.push('Photo', {
            breed
        });
    }

    const onRefresh = () => {
        loadBreedDogs();
    }

    const renderItem = ({ item }) => {
        return (
            <Fragment>
                <TouchableOpacity
                    onPress={() => handlePress(item.value)}>
                    <View style={{ height: 24 }} />
                    <Text style={styles.title}>{item.name}</Text>
                    <View style={{ height: 12 }} />

                </TouchableOpacity>
                <View style={{ height: 1, backgroundColor: '#bbb', width: width - 80 }} />
            </Fragment>
        )
    }

    return (
        <View style={styles.root}>
            <View style={styles.list}>
                <FlatList
                    refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}
                    data={listDogs}
                    renderItem={renderItem}
                    keyExtractor={(breed) => breed.id}
                    style={styles.inbox} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        paddingLeft: 16,
        backgroundColor: '#FFF'
    },
    list: {
        width,
        height: height - 100,
        paddingBottom: 20,
        paddingHorizontal: 20,
    },
    title: {
        fontSize: 18,
        color: '#000',
        letterSpacing: .5
    }
});

export default Main;
