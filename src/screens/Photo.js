import React, { useEffect, useState } from 'react';
import MasonryList from "react-native-masonry-list";

const Photo = ({ ...props }) => {

    const breed = props.route.params.breed;
    const [dogsImages, setDogsImages] = useState([]);

    useEffect(() => {
        loadImageDogs();
        return () => {
            console.log('unmount');
        }
    }, []);

    const loadImageDogs = async () => {
        console.log(breed);
        try {
            console.log(`https://dog.ceo/api/breed/${breed}/images`);
            let response = await fetch(`https://dog.ceo/api/breed/${breed}/images`, {
                method: 'GET'
            });
            let result = await response.json();
            if (result.status === 'success') {
                let arrayImages = [];
                for (const element of result.message) {
                    arrayImages.push({ url: element });
                }
                setDogsImages(arrayImages);
            } else {
                setDogsImages([]);
                console.log("Error getting documents:");
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <MasonryList
            images={dogsImages}
        />
    );
};

export default Photo;
